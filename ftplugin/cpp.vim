source $HOME/.vim/coc_config 
nnoremap <F3> : HeaderguardAdd<CR>
let g:clang_format#auto_format = 1
" CLangFormatter config
nnoremap <leader>f :ClangFormat<CR>

function! g:HeaderguardName()
    let prefix = "" 
    if getcwd() !=? expand("%:p:h:h")
        let prefix = toupper(expand("%:p:h:t")) . '_'
    endif
    return prefix  . toupper(expand('%:t:gs/[^0-9a-zA-Z_]/_/g'))
endfunction

function! GET_LINE(line_no)
    return substitute(substitute(getline(a:line_no),"\s+$", '', 'g'), "^\s+",'','g')
endfunction

let s:last_moved_by = 0

function! PRINT_LINE(line_no, val, lastln)
    if a:line_no <= a:lastln
        call setline(a:line_no, a:val)
    else
        call append(a:lastln + s:last_moved_by, a:val)
        let s:last_moved_by += 1
    endif
endfunction

" Example input data:
" result::expected<hash> //[return type]
" load_object   //[function name]
" const constext& // [param present in all calls]
" const db::hash& //[param present in all calls]
" --- // separator, function will loop over elements after it and create
"  //MOCK_METHOD call for each element
" commit
" image
" image_storage
" 
" Valid inputs:
" result::expected<void>
" do_something
" int
" float
" ---
"
" result::expected<void>
" do_something
" int
" float
"
" result::expected<void>
" do_something
" int
" float
" ---
" int
" double
" std::string
" Invalid inputs:
" result::expected<void>
" do_something
" int // minimum 4 lines are required to be processed

function! GENMOCK() range
    if a:lastline < 4
        return
    endif
    let l:return_type = GET_LINE(a:firstline)
    let l:fun_name = GET_LINE(a:firstline+1)
    let l:currline = a:firstline + 2
    let l:args = []
    let l:variants = []
    let l:line = GET_LINE(l:currline)
    while match(l:line,"---") == -1 && l:currline <= a:lastline
        let l:currline += 1
        let l:args +=  [l:line]
        let l:line = GET_LINE(l:currline)
    endwhile
    let l:currline = l:currline+1
    if l:currline <= a:lastline
        for lineno in range(l:currline, a:lastline)
            let l:variants += [GET_LINE(lineno)]
        endfor
    endif
    let l:currline = a:firstline
    if empty(l:variants)
       call PRINT_LINE(l:currline, "MOCK_METHOD(" .l:return_type .",",a:lastline) 
       call PRINT_LINE(l:currline+1, "\t" . l:fun_name . ",", a:lastline)
       call PRINT_LINE(l:currline+2, "\t(",a:lastline)
       let l:currline = l:currline + 3
           for arg in l:args
                call PRINT_LINE(l:currline, "\t" . arg, a:lastline)
                let l:currline = l:currline + 1
           endfor
        call PRINT_LINE(l:currline+1, "\t));\n", a:lastline)
    else
        for v in l:variants
           call PRINT_LINE(l:currline, "MOCK_METHOD(" .l:return_type .",",a:lastline) 
           call PRINT_LINE(l:currline+1, "\t" . l:fun_name . ",", a:lastline)
           call PRINT_LINE(l:currline+2, "\t(",a:lastline)
           let l:currline = l:currline + 3
               for arg in l:args
                    call PRINT_LINE(l:currline, "\t" . arg, a:lastline)
                    let l:currline = l:currline + 1
               endfor
               call PRINT_LINE(l:currline, "\t" . v, a:lastline)
               call PRINT_LINE(l:currline+1, "\t));\n", a:lastline)
               let l:currline += 2
            endfor
    endif
    let s:last_moved_by = 0
    if exists('g:loaded_clang_format')
        ClangFormat
    endif

endfunction

command -range GENMOCK <line1>,<line2>call GENMOCK()

map <leader>m GENMOCK<CR>
