source $HOME/.vim/coc_config
nmap <F9> :w <CR> :!clear; python % <CR>
" autocmd BufWritePre *.py execute ':Black'
let g:python_highlight_all = 1
autocmd FileType py,python setlocal colorcolumn=88
autocmd FileType py,python match ErrorMsg '\%>88v.\+'

"let g:formatdef_autopep8 = '"autopep8 --experimental --max-line-length 79 -"'
let g:formatters_python = ['black']

" au BufWrite *.py :Autoformat
