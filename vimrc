" - Avoid using standard Vim directory names like 'plugin'
filetype plugin indent on
set showcmd
" Initialize plugin system
 call plug#begin('~/.vim/plugged')

" Plug 'valloric/youcompleteme'
Plug 'morhetz/gruvbox'
Plug 'scrooloose/nerdcommenter'
Plug 'rhysd/vim-clang-format', {'for': ['cpp', 'proto']}
Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdtree'
Plug 'drmikehenry/vim-headerguard', {'for': 'cpp'}
" Plug 'tpop/vim-surround'
"Plug 'sirver/ultisnips'
" Plug 'honza/vim-snippets'
Plug 'neoclide/coc.nvim', {'for': ['cpp','c', 'python', 'javascript', 'rust', 'yaml'], 'branch': 'release'}
" Plug 'psf/black', {'for': 'python'}
Plug 'sheerun/vim-polyglot'
Plug 'prettier/vim-prettier', { 'for': 'javascript', 'do': 'yarn install' }
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'nvie/vim-flake8', { 'for': 'python' }
Plug 'majutsushi/tagbar'
Plug 'jackguo380/vim-lsp-cxx-highlight', {'for': ['cpp']}
Plug 'rrethy/vim-hexokinase'
Plug 'mattn/emmet-vim'
Plug 'junegunn/fzf.vim'
Plug 'rust-lang/rust.vim', {'for': ['rust']}
Plug 'Yggdroot/indentLine'
" Plug 'peterhoeg/vim-qml'
call plug#end()

" Core vim config
set number
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
let mapleader = ','
set clipboard=unnamed
set splitright
set splitbelow
set mouse=a
set relativenumber
filetype plugin on

" Scheme
syntax enable
set bg=dark
colorscheme gruvbox

" Highlight long lines
match ErrorMsg '\%>80v.\+'
" Show 80 char col
set colorcolumn=81
" HeaderguardAdd
"NerdTree config
" launch Nerdtree automatically upon vim start
autocmd vimenter * NERDTree | wincmd w
" close vim if the only window open is nerd tree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") 
             \&& b:NERDTree.isTabTree()) | q | endif
map <C-p> :NERDTreeToggle<CR>
